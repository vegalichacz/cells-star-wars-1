'use strict';

module.exports = config => {
  return {
    tag: "cells-template-paper-drawer-panel",
    properties: {
      disableEdgeSwipe: true,
      headerFixed: true,
      hasFooter: false
    }
  }
};
