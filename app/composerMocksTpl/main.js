'use strict';

module.exports = config => {
  const page = 'main';
  const template = require('./commons/template')();

  const pageTemplate = {
    zone: 'app__main',
    type: 'UI',
    familyPath: `../elements/pages/${page}-page`,
    tag: `${page}-page`,
    properties: {
      cellsConnections: {
        in: {
          [`species_loaded_ch`]: {
            bind: 'speciesLoadedAction'
          }
        },
        out: {
          ['$_navigation_chanel']: {
            bind: 'avatar-change',
            link: {
              page: 'avatar'
            }
          },
          ['save_name_ch']: {
            bind: 'save-name'
          }
        }
      }
    }
  };

  const mainDM = {
    zone: 'app__main',
    type: 'DM',
    familyPath: `../elements/data-managers/${page}-dm`,
    tag: `${page}-dm`,
    properties: {
      cellsConnections: {
        in: {
          ['save_name_ch']: {
            bind: 'saveName'
          }
        },
        out: {}
      }
    }
  };

  const secondaryDM = {
    zone: 'app__main',
    type: 'DM',
    familyPath: `../elements/data-managers/secondary-dm`,
    tag: `secondary-dm`,
    properties: {
      cellsConnections: {
        in: {
          ['__bridge_evt_page-ready']: {
            bind: 'loadSpecies'
          }
        },
        out: {
          [`species_loaded_ch`]: {
            bind: 'species-loaded'
          }
        }
      }
    }
  };

  return {
    template,
    components: [
      pageTemplate,
      mainDM,
      secondaryDM
    ]
  }
};
