{
  class SecondaryDM extends CellsBehaviors.CellsManageStorage(Polymer.Element) {
    static get is() {
      return 'secondary-dm';
    }

    static get properties() {
      return {};
    }

    constructor() {
      super();
    }

    loadPlanets(data = {}) {
      let { next = 0 } = data;
      let element = 'planets';
      let event = 'planets-loaded';
      this._generateRequest(element, next, event)
    }

    loadSpecies(data = {}) {
      let { next = 0 } = data;
      let element = 'species';
      let event = 'species-loaded';
      this._generateRequest(element, next, event)
    }

    _generateRequest(element, next = 0, ev) {
      let httpService = this.$.httpService;
      httpService.set('url', `https://swapi.co/api/${element}/`);
      if (next !== 0) {
        httpService.set('params', {'page': next})
      }

      httpService.generateRequest().completes.then(response => {
        this.dispatchEvent(new CustomEvent(ev, {
          bubbles: true,
          composed: true,
          detail: response.get('xhr').response
        }));
      });
    }
  }
  customElements.define(SecondaryDM.is, SecondaryDM);
}
