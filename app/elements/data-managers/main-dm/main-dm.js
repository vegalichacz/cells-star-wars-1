{
  class MainDM extends CellsBehaviors.CellsManageStorage(Polymer.Element) {
    static get is() {
      return 'main-dm';
    }

    static get properties() {
      return {
        name: {
          type: String,
          value: ''
        }
      }
    }

    constructor() {
      super();
    }

    saveName(data){
      console.log('Data', data);
      this.manageStorage('set', 'name', data.name, 'local');
    }

    retrieveName() {
      return this.manageStorage('get', 'name', '', 'local');
    }
  }

  customElements.define(MainDM.is, MainDM);
}
