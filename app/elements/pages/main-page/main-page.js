{
  class MainPage extends Polymer.mixinBehaviors([
    CellsBehaviors.i18nBehavior
  ], Polymer.Element) {

    static get properties() {
      return {
        name: {
          type: String,
          value: ''
        },
        results: {
          type: Array,
          value: [],
          notify: true
        }
      }
    }

    static get is() {
      return 'main-page';
    }

    constructor() {
      super();
    }

    goToAvatar(ev) {
      this.dispatchEvent(new CustomEvent('avatar-change',{
        bubbles: true,
        composed: true,
        detail: {}
      }));
    }

    saveName(ev) {
      console.log(this.name);
      this.dispatchEvent(new CustomEvent('save-name', {
        bubbles: true,
        composed: true,
        detail: {
          name: this.name
        }
      }))
    }

    speciesLoadedAction(data) {
      let { results } = data;
      this.set('results', results);
    }
  }

  customElements.define(MainPage.is, MainPage);
}
