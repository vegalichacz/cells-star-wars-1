{
  const CellsPage = customElements.get('cells-page');

  class AvatarPage extends Polymer.mixinBehaviors(
    [CellsBehaviors.i18nBehavior],
    CellsPage) {

    static get is () {
      return 'avatar-page';
    }

    static get properties() {
      return {
        pageNumber: {
          type: Number,
          value: 1
        },
        results: {
          type: Array,
          value: [],
          notify: true,
          observer: 'observeChange'
        }
      };
    }

    observeChange(newValue, oldValue) {
      console.log('Old ->', oldValue);
      console.log('New ->', newValue);
    }

    constructor(){
      super();
      console.log(this.t);
    }

    ready() {
      super.ready();
      setTimeout(() => this.pageNumber = 2, 2000);
      this._getManagerName();

      this._handleDM();
      this._handleConnections();
    }

    _handleDM() {
      const secondaryDM = this.$.secondaryDM;
      this.publishOn('planets_loaded_ch', secondaryDM, 'planets-loaded');
    }

    _handleConnections() {
      this.subscribe('planets_loaded_ch', (data) => this._onPlanetsLoaded(data));
    }

    _onPlanetsLoaded(data = {}) {
      let planetArray = this.get('results');
      let { results = [], next } = data;
      this.set('results', [...planetArray, ...results]);
      this.next = next.slice(-1);
      this.loading = false;
    }

    onPageEnter() {
      this._getName();
      this._retrievePlanets();
    }

    getMorePlanets() {
      this.loading = true;
      const DM = this.$.secondaryDM;
      DM.loadPlanets({next: this.get('next')});
    }

    _retrievePlanets() {
      const DM = this.$.secondaryDM;
      DM.loadPlanets();
    }

    goToMain() {
      this.navigate('main');
    }

    _getManagerName() {
      const DM = this.$.mainDM;
      console.log(DM.retrieveName());
    }

    _getName() {
      this.name = this.t('welcome', '', {showName: this.$.mainDM.retrieveName()});
    }
  }
  customElements.define(AvatarPage.is, AvatarPage);
}
