(function(document) {
  'use strict';

  window.CellsPolymer.start({
    routes: {
      'main': '/',
      'avatar': '/avatar'
    }
  });

}(document));
